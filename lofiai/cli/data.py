import click
from datetime import datetime
import os
import subprocess
import re
import requests
from spotdl.authorize.services import AuthorizeSpotify
from spotdl.metadata_search import MetadataSearch
from spotdl.metadata.exceptions import SpotifyMetadataNotFoundError
from bs4 import BeautifulSoup

from ..lib import AliasedGroup

def spotdl_cmd(args):
    return 'yes N | spotdl --trim-silence --no-spaces -f {artist}-{track-name}.{output-ext} --search-format="{artist} - {track-name}" ' + args


@click.group(cls=AliasedGroup)
def data():
    """Tools for scraping, downloading, and verifying data."""
    pass


@data.group(cls=AliasedGroup)
def youtube():
    """Youtube tools."""
    pass


@data.group(cls=AliasedGroup)
def spotify():
    """Spotify tools."""
    pass


@click.command()
@click.argument('url')
def desc_links(url):
    """Navigate all redirect links in a youtube description. Prints targets."""
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    description_redirects = soup.select('a[href*=redirect]')
    for elem in description_redirects:
        response = requests.get('https://www.youtube.com' + elem.get('href'))
        soup = BeautifulSoup(response.text, 'html.parser')
        # Grab the link from the "Go to Site button"
        response = requests.get(soup.find(id='invalid-token-redirect-goto-site-button').get('href'))
        print(response.url)
youtube.add_command(desc_links)


@click.command()
@click.argument('path', type=click.Path(exists=True))
def dl_timestamp_names(path):
    """Downloads a list of song names in a given file using spotdl.

    The file format is a list of timestamps and song name pairings which is
    typical on Youtube descriptions and comments. e.g.

    ```

    0:00 idealism - Both of Us
    2:10 Keem the Cipher - [BLOSSOM.] (w sugi.wa)
    3:58 Jhfly - Crossings

    ```

    This will call something like `spotdl -s "idealism - Both of Us"`.
    This mostly exists because I don't want to write Bash to
    do this.
    """
    with open(path) as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue

            # Drop the timestamp.
            song_desc = ' '.join(line.split()[1:])
            os.system(spotdl_cmd(f'-s "{song_desc}"'))
youtube.add_command(dl_timestamp_names)


@click.command()
@click.argument('path', type=click.Path(exists=True))
def dl_list(path):
    os.system(spotdl_cmd(f'--list "{path}"'))
spotify.add_command(dl_list)


@click.command()
@click.argument('mp3s', type=click.Path(exists=True), nargs=-1)
def verify_mp3(mp3s):
    """Ensures that a given mp3's length approximatedly equals the spotify metadata length."""
    AuthorizeSpotify(
        # Credentials from spotdl.
        client_id='4fe3fecfe5334023a1472516cc99d805',
        client_secret='0f02b7c483c04257984695007a4a8d5c',
    )

    for mp3 in mp3s:
        file_duration_info = subprocess.run(f"ffprobe '{mp3}' 2>&1 | grep Duration | awk " + "'{print $2}'", capture_output=True, shell=True)
        # Value looks like: "00:01:56.64,". Drop the millisecond timing.
        duration_info = file_duration_info.stdout.decode().strip()[:-4]
        if not duration_info:
            print(f'No duration found in {mp3} metadata.')
            continue
        try:
            dur = datetime.strptime(duration_info, '%H:%M:%S')
        except Exception as e:
            print(f'Faulty duration metadata from {mp3}: {duration_info}')
            continue

        dur_seconds = dur.hour * 60 * 60 + dur.minute * 60 + dur.second

        file_comment_info = subprocess.run(f"ffprobe '{mp3}' 2>&1 | grep comment | head -n 1 | awk " + "'{print $3}'", capture_output=True, shell=True)
        spotify_url = file_comment_info.stdout.strip()
        if not spotify_url:
            print(f'No spotify url found in {mp3} comment metadata.')
            continue
        search_metadata = MetadataSearch(
            spotify_url.decode(),
            lyrics=False,
            yt_search_format='{artist} - {track-name}',
            yt_manual=False,
        )
        try:
            t = search_metadata.on_spotify()
        except Exception as e:
            print(e)
            continue

        threshold = 10
        if abs(t['duration'] - dur_seconds) > threshold:
            print(f'{mp3} has a duration of {dur_seconds}s while Spotify metadata suggests a duration of {t["duration"]}s')

spotify.add_command(verify_mp3)


