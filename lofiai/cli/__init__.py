import click

from ..lib import AliasedGroup
from .data import data

@click.group(cls=AliasedGroup)
def cli():
    """Program description goes here."""
    pass

cli.add_command(data)
