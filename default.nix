{ pkgs ? import <nixpkgs> {}
, pythonPackages ? pkgs.python37Packages }:

let
  # f would be what would go in nixpkgs. Which explains the strange style.
  f =
    { buildPythonApplication
    , pytest
    , click, selenium
    }:

    buildPythonApplication rec {
      version = "master";
      pname = "lofiai";
      src = ./.;

      doCheck = false;
      checkInputs = [ pytest ];
      propagatedBuildInputs = [
        click
        selenium
      ];
    };
  drv = pythonPackages.callPackage f {};
in
  drv
